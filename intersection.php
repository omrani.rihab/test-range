<!DOCTYPE html>
<html>

<head>
  <title>Test</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>


<?php

$res = array();
$arr = array();

//Create an array containing a range of elements.
function rangeArray($array)
    {
    $newArray = array(); 
    foreach ($array as  $arr) {
       
        $first = array_shift($arr);
        $last = array_pop($arr);
        $newArray [] = range($first,$last);    
   }
       
    return($newArray);
}

//array_intersect — Return the intersection of arrays
function getIntersection($nums) {
    $result = [];

    for ($i = 0; $i < count($nums) - 1; $i++) {
        $intersection = array_intersect($nums[$i], $nums[$i+1]);
        $result[] = $intersection;
    }

    return $result;
}

function getMin($arrayIntersection) {
    $minValue = null;

    foreach ($arrayIntersection as $subarray) {
        foreach ($subarray as $value) {
            if ($minValue === null || $value < $minValue) {
                $minValue = $value;
            }
        }
    }

    return $minValue;
}


function getMax($arrayIntersection) {
    $maxValue = null;

    foreach ($arrayIntersection as $subarray) {
        foreach ($subarray as $value) {
            if ($maxValue === null || $value > $maxValue) {
                $maxValue = $value;
            }
        }
    }

    return $maxValue;
}

$array = $_POST["array"][0];
$array = json_decode($array);

$arr = rangeArray($array);

// The array that need to be checked

$arr2 = array_filter(getIntersection($arr));
if(count($arr2) ==1 || count($arr2) == 0) {
   $arr2= array_unique($arr2);
   $arr2 = array_merge(...$arr2);

    $newArray2 = array();
    $newArray = array();

foreach ($arr as $subArray) {
    $newArray1 = array(); 
    
             if ((count(array_intersect($arr2, $subArray)) == count($arr2)) and (count(array_intersect($arr2, $subArray))>0)) {
                
                 $newArray2 = array_merge($newArray2, $subArray);

            } else {

                  $newArray1 = array(min($subArray), max($subArray));

            }
            if( count($newArray1) > 0 ) {
                $res = array_merge($res,$newArray1);
            }           
}
            if( count($newArray2) > 0 ) {
                array_push($newArray, min($newArray2), max($newArray2)); 
            }


            $res = array_merge($res,$newArray);

}  else {

$arrayMin= array();
$arrayMax= array();
$arrayIntersection= array();
$arrayMin1= array();
$arrayMax1= array();
$arrayTest = array();

foreach ($arr2 as $b) {
    $arrayIntersection= array();
    foreach ($array as $a) {
        $arrayIntersection1= array();
       
        if (array_intersect($b, $a) == $b and (count(array_intersect($b, $a))>0)) {
        
            $arrayIntersection[] = $a;
            
        }
       
        elseif ( count(array_intersect($b, $a) )== 0){ 
           
             $arrayIntersection1[] = $a;
        }
         
    }
        
        $minValue = getMin($arrayIntersection);
        $maxValue = getMax($arrayIntersection);
        $resultArrayIntersection =array($minValue,$maxValue);
        $res = array_merge($res,$resultArrayIntersection); 
       
       // print_r($res);
      
    }

}

?> 
<div class="container">
  <h2>Result is:</h2>
  
    <div class="form-group">
      <p><?php print_r($res);?></p>
    </div>
</div>
</body>
</html>

